Sistema de Lectura de Excel
======

## Version
* [Version 0.1]

## Download
```$ git clone https://github.com/evaldezdiaz/WorkingWithExcel.git
...```

## Contributors
- <a href="https://github.com/evaldezdiaz" target="_blank">Edgar Valdez</a>

## Specifications
- Language - Java
- IDE - Intellij Idea 2016.1.1
- JDK - 1.8
- DATA BASE - N/A
- OTHERS    - Excel File
- FRAMEWORKS - Spring 4.2.6 (Maven Project), JQuery y Bootstrap
