<%--
  Created by IntelliJ IDEA.
  User: EdgarValdez
  Date: 05/3/18
  Time: 12:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Proyecto Apache Poi</title>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="Calculadora de Excel" name="description" />
        <meta content="Edgar Valdez" name="author" />
        <link rel="stylesheet" href="../resources/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css"/>
        <link rel="stylesheet" href="../resources/css/styles.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img class="logo img-responsive img-circle" src="../resources/images/EVD%20Developers.jpg" alt="Logo">
                   </div>
                    <div class="col-md-6">
                        <div class="page-header">
                            <h1 class="titulo">Proyecto Apache Poi para Domingo</h1>
                        </div>
                        <div class="list-group">
                            <a href="" class="list-group-item">
                                <h4 class="list-group-item-heading">Cantidad:</h4>
                                <p class="list-group-item-text"># ${respuestaProcesoExcel.getCantidadArticulos()}</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="" class="list-group-item">
                                <h4 class="list-group-item-heading">Cantidad Total:</h4>
                                <p class="list-group-item-text"># ${respuestaProcesoExcel.getCantidadArticulosGeneral()}</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="" class="list-group-item">
                                <h4 class="list-group-item-heading">Valor Netos:</h4>
                                <p class="list-group-item-text"># ${respuestaProcesoExcel.getIngresosNetos()}</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="" class="list-group-item">
                                <h4 class="list-group-item-heading">Impuestos:</h4>
                                <p class="list-group-item-text"># ${respuestaProcesoExcel.getImpuestos()}</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="" class="list-group-item">
                                <h4 class="list-group-item-heading">Ingresos Brutos:</h4>
                                <p class="list-group-item-text"># ${respuestaProcesoExcel.getIngresosBrutos()}</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-default btn-lg btn-block " onclick="window.location.href='/procesarExcel.html'">
                        Procesar <span class="glyphicon glyphicon-refresh"></span>
                    </button>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <div>

        </div>
        <link rel="script" href="../resources/js/jquery-1.11.3.min.js"/>
        <link rel="script" href="../resources/js/bootstrap.min.js"/>
    </body>
</html>
