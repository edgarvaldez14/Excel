package edu.unapec.repositorios.interfaces;

import edu.unapec.entidades.Articulo;
import edu.unapec.respuestas.RespuestaProcesoExcel;

import java.util.List;

/**
 * Created by EdgarValdez on 05/3/18.
 */
public interface ArticuloRepIF {

    List<Articulo> obtenerArticulos();

    void escribirProcesoArticulos(RespuestaProcesoExcel articulosProceso);

}
