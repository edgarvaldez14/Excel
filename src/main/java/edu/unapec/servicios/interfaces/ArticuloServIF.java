package edu.unapec.servicios.interfaces;

import edu.unapec.entidades.Articulo;
import edu.unapec.respuestas.RespuestaProcesoExcel;

import java.util.List;

/**
 * Created by EdgarValdez on 05/3/18.
 */
public interface ArticuloServIF {

    List<Articulo> obtenerArticulos();

    void escribirProcesoArticulos(RespuestaProcesoExcel articulosProceso);

    RespuestaProcesoExcel procesarValoresExcel();
}
